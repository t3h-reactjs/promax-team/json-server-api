const users = require('./config-server/tb-user');
const db = require("./config-server/tb-db")
const cv = require('./config-server/tb-cv');
const contact = require("./config-server/tb-contact")

module.exports = () => ({
    users,
    db,
    contact,
    cv
});